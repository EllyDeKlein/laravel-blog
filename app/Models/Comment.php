<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = ['title','content','article_id']; //article id hier ook?

    public function article() {

        return $this->belongsTo(Article::class); //De relatie met de database maken enzv. zodat ik daarna kan gebruiken in Cotnroller.
    }
}
