<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Observers\ArticlesObserver;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Article extends Model
{


   

    public static function boot()
    {
        parent::boot();
        self::observe(new ArticlesObserver);
    }

 

    use HasFactory;
    protected $fillable = ['title','content','user_id','image']; 

    public function user() {

        return $this->belongsTo(User::class); 
    }
    public function categories() {
        return $this->belongsToMany(Category::class, 'article_category');
        //return $this->belongsToMany(Category::class)->withPivot(['category_id']) ;
        
    }
    public function comments() {
        return $this->hasMany(Comment::class); 
    }


    public function scopeWithFilters($query)
    {

        return $query->when(count(request()->input('categories', [])), function ($query) {
            $query->whereIn('category_id', request()->input('categories'));

    });

}
}