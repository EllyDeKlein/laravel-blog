<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(Request $request)
    {
        $comment = new Comment($this->validateComment());
        $comment->article_id = $request->input('article_id');
        $comment->save();
        return back();  
    }
    public function validateComment() {
        return request()->validate([
            'title'     =>    ['required', 'min:2'],
            'content'   =>    ['required', 'min:4']
        ]);
        }
}