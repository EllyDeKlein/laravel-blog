<?php

namespace App\Http\Controllers;
use Laravel\Cashier\SubscriptionBuilder\RedirectToCheckoutResponse;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
 
    public function index()
    {
        if (Auth::check()) {
        return view('subscribe.index');
        } else {
            return redirect('/login')->with('message', 'Log eerst in of word gratis lid om je aan te melden voor de premium lidmaatschap!');
        }
    }

    public function pay()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $redirect = $user->newSubscriptionViaMollieCheckout('main', 'premiumblog')->create();
            return $redirect;
        } else {
            return redirect('/login');
        }
    }
    public function thanks()
    {
        
        return view('subscribe.thanks');
    }
    
}
