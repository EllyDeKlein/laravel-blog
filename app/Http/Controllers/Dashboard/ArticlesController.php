<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Support\Facades\Auth;
use App\Models\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class ArticlesController extends Controller
{
    public function index()
  
    {      
        $user =Auth::user();
        $data['articles'] = $user->articles()->get();
        return view('dashboard.index', $data); 
    }
 
    public function create()
    {
        $data['categories'] = Category::all();  
        return view('articles.create', $data);
    }

    public function store(Request $request) 
    {
       $article = new Article($this->validateArticle());
       $article->user_id = Auth::user()->id;
       if ($request->hasFile('image')) {
            $filename = $request->image->getClientOriginalName(); 
            $article->image = $request->file('image')->storeAs('image', $filename, 'public');
            $article->update(['image' => $filename]);
       } else {
            echo 'artikel zonder image';
            $article->update(['image' => "9"]);          
       }
       if(isset($request->premium)) { $article->premium = 1; } else { $article->premium = 0; }

       $article->save();
       $article->categories()->attach(request('categories'));
       return redirect(route('dashboardarticles.index'));
    }

    public function edit(Article $article)
    {
        $data['article'] = $article;
        return view('dashboard.edit', $data); 
    }

    public function update(Request $request, Article $article)
    {
        $article->update($this->validateArticle());
        return redirect(route('dashboardarticles.index'));
    }

    public function destroy(Article $article)
    {
        $article->delete();
        return redirect(route('dashboardarticles.index'));  
    }
    public function validateArticle() {
        return request()->validate([
            'title' => ['required', 'min:2'],
            'content' =>['required', 'min:4']
        ]);
}
}