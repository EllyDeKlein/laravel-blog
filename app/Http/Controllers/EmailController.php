<?php

namespace App\Http\Controllers;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
//use Notification;

use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function index()
    {
        return view('emails.test'); 
    }

    public function store(Request $request)
    {
        $users = \App\Models\User::all();  
        request()->user()->notify(new \App\Notifications\ArticlesNotification($users)); //Waar moet ik dit plaatsen? Functie naam hoeft geen store te zijn? Eigen ?
        return view('emails.newsletter'); 
    }

    public function newsletter(Request $request)
    {
            $user = \App\Models\User::find(\Auth::user()->id);    //ik denk $user = \Auth.. en dan $request->update($user->newsletter => 1) zoiets
            $user->newsletter = 1;
            $user->save();
            return redirect(route('articles.index'));   
    }
    
}
