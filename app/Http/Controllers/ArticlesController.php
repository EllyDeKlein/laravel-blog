<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Article;
use App\Models\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function index(Request $request)
    {
        $data['categories'] = Category::all(); 
        $data['articles']   = Article::where('premium',0)->orderBy('created_at', 'desc')->get();
        return view('articles.index', $data); //, 'categoryId' => $categoryId
    }
    public function category(Request $request)
    {
       // $data['articles']   = Article::where('premium',0)->orderBy('created_at', 'desc')->get();
        $data['categories'] = Category::all();
        $user = \Auth::user();
        $category_ids = collect(explode(',', $request->category_ids))
        ->map(fn($i) => trim($i))
        ->all();
        if ($request->category_ids == !null) {
           
            $data['articles'] = Article::whereHas('categories', fn($query) => 
            $query->whereIn('categories.id', $category_ids)
                ->where('articles.premium', 0)
            )->get();
        } else {
            $data['articles']  = Article::where('premium',0)->orderBy('created_at', 'desc')->get();
        }
   
        if($user = \Auth::check() && $user->subscribed('main'))   {
            $data['articles'] = Article::whereHas('categories', fn($query) => 
            $query->whereIn('categories.id', $category_ids)
                ->where('articles.premium', 1 and 0)
        )->get();
        }
         return view('articles.render', $data);
    } 

    public function create() 
    {
        
            return view('articles.create', ['categories' => Category::all()]);
    }
    public function show(Article $article)
    {
            $data['article'] = $article;
            return view('articles.show',$data);
    }

    public function showPremium(Request $request    )
    
    {
        $user = \Auth::user();
        $data['categories'] = Category::all();
     
        if ( $user = \Auth::check() && $user->subscribed('main'))  
        {
            $data['articles']   = Article::where('premium',1)->orderBy('created_at', 'desc')->get();
            return view('articles.premium', $data);
    
        }else{
            return redirect('/subscribe')->with('message', 'Abboneer je hier op Premium!');
    }
    }
}
