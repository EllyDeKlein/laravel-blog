<?php

namespace App\Console\Commands;

use App\Article;
use App\Notifications\ArticlesNotification;
use App\User;
//use Illuminate\Notifications\Notification;
//use Notification;
use Illuminate\Support\Facades\Notification;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;



class SendNewArticlesEmail extends Command
{
    protected $signature = 'articles:bundel';

    protected $description = 'Send emails to users with list of new articles';
    //zaten 2 handles misschien moet ik deze ff op nieuw maken. 

    //   public function __construct()
    //    {
    // parent::__construct(); **Standaard zat dat erbij
    //   }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $articles = \App\Models\Article::whereBetween('created_at', [now()->subDay(), now()])->get(); //hier zat ->get() 8er //moet ik hier de collections mappen ?
            //moet ik mappen?
        $users = \App\Models\User::where('newsletter', 1)->get();

        //alt shift f 
        Log::info("Mail info:");
        Notification::send($users, new \App\Notifications\ArticlesNotification($articles));
        //$users

    }
}
