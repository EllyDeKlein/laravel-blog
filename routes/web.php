<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticlesCategoriesController;
Route::redirect('/', '/articles'); 

// Articles
Route::get('/articles',                                 'App\Http\Controllers\ArticlesController@index')
    ->name('articles.index');
Route::get('/articles/category/',          'App\Http\Controllers\ArticlesController@category')
    ->name('articles.category');
Route::get('/articles/create',                          'App\Http\Controllers\ArticlesController@create')
    ->name('articles.create');
//Email
Route::post('/articles/newsletter/register',            'App\Http\Controllers\EmailController@newsletter')
    ->name('articles.registernewsletter');
Route::get('/articles/newsletter',                      'App\Http\Controllers\EmailController@store')
    ->name('articles.newsletter');
//Comments
Route::get('/articles/{article}/comments',              'App\Http\Controllers\ArticlesController@show')
    ->name('articles.show');
Route::post('/comments',                                'App\Http\Controllers\CommentsController@store')
    ->name('newcomment.store');
//Dashboard --moet middleware overal waar je ingelogd hoor te izjn of alleen voor het inloggen zelf?
Route::get('/dashboard/articles/{articles}/edit',       'App\Http\Controllers\dashboard\ArticlesController@edit')
    ->name('articles.edit') 
    ->middleware('auth');
Route::post('/articles',                                'App\Http\Controllers\dashboard\ArticlesController@store')
    ->name('articles.store');
Route::match(['put', 'patch'],'/articles/{articles}',   'App\Http\Controllers\dashboard\ArticlesController@update')
    ->name('articles.update');
Route::get('/dashboard/articles',                       'App\Http\Controllers\dashboard\ArticlesController@index')
    ->name('dashboardarticles.index')
    ->middleware('auth');
Route::delete('/dashboard/articles/{article}',          'App\Http\Controllers\dashboard\ArticlesController@destroy')
    ->name('articles.destroy'); 
Route::get('/auth',                                     'App\Http\Controllers\dashboard\ArticlesController@index')
    ->name('articles.login')
    ->middleware('auth');
//Axios
Route::get('/axios',                                    'App\Http\Controllers\AxiosController@index')
    ->name('axios.index');
Route::get('/welcome', function()  {
   return view('welcome');
});

Route::resource('articles.categories', ArticlesCategoriesController::class);
/////////////////

Route::get('/categories/{id}', [ 'uses' =>              'App\Http\Controllers\CategoriesController@index']);
//Payment Mollie and Premium section
Route::get('/articles/premium',                         'App\Http\Controllers\ArticlesController@showPremium')
    ->name('articles.premium');
    //hier nog middleware plus test
Route::get('/subscribe',                                'App\Http\Controllers\SubscriptionController@index')
    ->name('subscribe.index');
Route::get('/subscribe/pay',                            'App\Http\Controllers\SubscriptionController@pay')
    ->name('subscribe.pay');
Route::get('/subscribe/thanks',                         'App\Http\Controllers\SubscriptionController@thanks')
    ->name('subscribe.thanks');
Auth::routes(); //of moeten alle authentication routes hier in? Dit kwam bij Vue npm install ding erbij