<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id('id'); 
            $table->unsignedBigInteger('user_id'); 
            $table->timestamps();
            $table->string('title');
            $table->text('content');
            $table->string('image')->nullable();
            $table->boolean('premium')->default(0);
            $table->foreign('user_id') //Hij ziet hier dus van userS user..
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
