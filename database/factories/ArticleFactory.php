<?php

namespace Database\Factories;
use App\Models\Article; //Ik veranderde deze naar article maar dat deed niks.
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;


class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' =>User::factory(), //hier aangepast om te kijken of werkt factory(\app\Models\User::class)
            'title' => $this->faker->sentence(10),
            'content' => $this->faker->paragraph
        ];
    }
}
