
require('./bootstrap');
const catfilter = document.getElementById('categories');
function getSelectValues(select) {
  var result = [];
  var options = select && select.options;
  var opt;
  for (var i=0, iLen=options.length; i<iLen; i++) {
    opt = options[i];

    if (opt.selected) {
      result.push(opt.value || opt.text);
    }
  }
  return result;
}
//window. om de global scope te kunnen bereiken, dit was even een workaround van Jeroen. Maar er klopt iets niet.
window.getArticles =  function() {
    let res = axios({ 
        method: 'get',
        url: `/articles/category/`,
        json: true,
        params: {
          category_ids:  getSelectValues(catfilter).join(',') 
         }
    })
    .then(res => document.getElementById('blogContainer').innerHTML = res.data) 
    .catch(err => console.error(err));
  
}


