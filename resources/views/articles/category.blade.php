@for($i=0;$i<count($articles);$i++)
  <div class="card" style="width: 40rem;">
    @if($articles[$i]->image != null)
      <img src="{{asset('/storage/' . $articles[$i]->image)}}" class="card-img-top" alt="...">
    @endif
    <div class="card-body">
      <h6 class="card-subtitle mb-2 text-muted"><strong>Date</strong> {{$articles[$i]->created_at}}</h6>
      <h6 class="card-subtitle mb-2 text-muted"><strong>Author:</strong> {{$articles[$i]->user_name}}</h6>
      <h5 class="card-title">{{$articles[$i]->title}}</h5>
      <p class="card-text">{{$articles[$i]->content}}</p>
      <a href="" class="btn btn-primary">Reacties weergeven </a>
    </div>
  </div>
@endfor