@extends ('layouts/app')
@section ('content')
    <h1>Nieuw blog bericht</h1>
    <form method ="POST" action="/articles" enctype="multipart/form-data">
        @csrf 
        <div class="field">
            <label class="label" for="title">Title</label>
        <div class ="control">
                <input class="input" type="text" name="title" id="number" value="Titel" required>
                @if ($errors->has('title'))
                    <p>{{$errors->first('title')}}</p>
                @endif 
        </div>
        <div class="field">
            <label class="label" for="content">Content</label>
        <div class ="control">
            <textarea id="content" name="content" rows="4" cols="50">Typ je blog !</textarea>
                @if ($errors->has('content'))
                    <p>{{$errors->first('content')}}</p>
                @endif 
        </div>
        </div>
        <div class ="control">
                <input class="input" type="file" name="image" id="image" value="image">
                @if ($errors->has('title'))
                    <p>{{$errors->first('title')}}</p>
                @endif 
        </div>
        <input type="checkbox" name="premium"/>
        <p>Premium Artikel</p>
        <div class="field">
            <label class="label" for="content">Categories</label>
        <div class ="control">
            <select 
            name="categories[]"
            multiple
            >
            @foreach ($categories as $category)
            <option value="{{$category->id}}">{{ $category->name}}</option>
            @endforeach
            </select>
                @if ($errors->has('categories'))
                    <p>{{$errors->first('categories')}}</p>
                @endif 
        </div>
        </div>
        <button type="submit">Voeg toe</button>
    </form>
@endsection