@extends ('layouts/app')
@section ('content')
  <h1> Premium Artikelen </h1>
@auth
  <p>(Logged in {{Auth::user()->name}} )</p>
@endauth
<hr>
<hr><hr<hr>
    <div id="app"> 
      <h1> Filter </h1>
    <div class="field">
            <label class="label" for="content">Zoek op categorie</label>
        <div class ="control">
            <select id="categories" name="categories[]" multiple>
            @foreach ($categories as $category)
              <option onclick="getArticles()" value="{{$category->id}}">{{ $category->name}}</option>
            @endforeach
            ></select>
              @if ($errors->has('categories'))
              <p>{{$errors->first('categories')}}</p>
              @endif 
        </div>
        </div>
</div>
  <hr><hr><hr>
    <div id="blogContainer">
       @include('articles.render')
    </div>
@endsection

