@extends ('layouts/app')
@section ('content')
    <h1 class="text-success"> Artikel + Reacties </h1> 
   
    <div class="card">
    <div class="p-3 mb-2 bg-info text-white">
        <div class="card-header">
        {{$article->title}}
        </div>
        </div>
        <div class="card-body">
            <p class="card-text">{{$article->content}}</p>
        </div>
    </div>
    
    <br><br><br><h2>Reacties:</h2>
    @foreach($article->comments as $comment)
    <div class="rounded">
    <div class="card"> 
    <div class="p-3 mb-2 bg-dark text-white">
        <div class="card-header">
       {{$comment->title}}
        </div>
        </div>
  <div class="card-body">
  
    <blockquote class="blockquote mb-0">
      <p>  {{$comment->content}}</p>
      <footer class="blockquote-footer">
      @if (Auth::check()) 
      Van: {{Auth::user()->name}} 
      @else 
      Van: User/Anonymous</footer>
      @endif
    </blockquote>
  </div>
</div>
</div>
    @endforeach
    <div style= "border: solid 2px; background-color:skyblue">
    <form method ="POST" action="/comments">
        @csrf 
        <div class="field">
            <label class="label" for="title">Title</label>
        <div class ="control">
             <input class="input" type="text" name="title" id="number" value="Titel" required>
             @if ($errors->has('title'))
             <p>{{$errors->first('title')}}</p>
             @endif 
        </div>
        <input type="hidden" name="article_id" id="post_id" value="{{ $article->id }}" />
        <div class="field">
            <label class="label" for="content">Content</label>
        <div class ="control">
            <textarea id="content" name="content" rows="4" cols="50">Commentaar hier</textarea>
             @if ($errors->has('content'))
             <p>{{$errors->first('content')}}</p>
             @endif 
        </div>
        </div>
        <button type="submit">Verstuur reactie @if (Auth::check()) als {{Auth::user()->name}} @endif</button>
    </form>
    </div>
@endsection

    
