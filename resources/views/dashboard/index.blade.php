@extends ('layouts/app')
@section ('content')
    <h1> Artikellen voor {{Auth::user()->name}}  </h1>
        @for($i=0;$i<count($articles);$i++)    
        <div class="card" style="width: 40rem;">
        @if($articles[$i]->image != null)
            <img src="{{asset('/storage/' . $articles[$i]->image)}}" class="card-img-top" alt="...">
            @endif
                <div class="card-body">
                <h6 class="card-subtitle mb-2 text-muted"><strong>Date</strong> {{$articles[$i]->created_at}}</h6>
                <h6 class="card-subtitle mb-2 text-muted"><strong>Author:</strong> {{$articles[$i]->user->name}}</h6>
                <h5 class="card-title">{{$articles[$i]->title}}</h5>
                <p class="card-text">{{$articles[$i]->content}}</p>
                <a href="{{ route('articles.show', $articles[$i]) }}" class="btn btn-primary">Reacties weergeven </a>
                </div>
                @for ($j=0;$j<count($articles[$i]->categories);$j++)
                <a href='#'>{{$articles[$i]->categories[$j]->name}}</a>
                @endfor
                
            <a href="{{ route('articles.edit', $articles[$i]) }}" class="btn btn-dark">  edit  </a>
                    <form method="POST" action="{{ route('articles.destroy', $articles[$i]) }}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                    </form>  
                    </div>
                </div>
                    <br>                                         
                @endfor        
@endsection