@extends ('layouts/app')
@section('content')
<div id='cssform'>
    <h1>Artikel aanpassen</h1>
        <form method ="POST" action="/aritcles/{{$article->id}}">  <?php //Browser only understands a POST request so de @method will make it a put...? ?>
        @csrf 
        @method('PUT')
        <div class="field">
            <label class="label" for="name">Title</label>
        <div class ="control">
             <input class="input" type="text" name="name" id="name" value="{{$article->title}}">
        </div>
        <div class="field">
            <label class="label" for="content">Content</label>
        <div class ="control">
             <textarea rows="8" cols="100"  value=>
             {{$article->content}}
             </textarea>
        </div>
        </div>
        <button type="submit">Bijwerken</button>
    </form>
</div>
<a href="{{ route('dashboardarticles.index')}}">Terug naar Artikelen overzicht</a>
@endsection