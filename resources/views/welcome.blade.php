@extends ('layouts/app')
@section ('content')
<h1>Mijn test page</h1>
<div id="app"> 
    <ol>
        <li v-if="articles.length>0" v-for="article in articles">
           @{{ article.title }}
        </li>
    </ol>
</div>
@endsection