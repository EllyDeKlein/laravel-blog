<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Blog!!!! App</title>
</head>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{ route('articles.index')}}">Overzicht</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    @if (Auth::check() && !Auth::user()->subscribed('main')) 
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('subscribe.index')}}">Word premium lid! <span class="sr-only">(current)</span></a>
      </li>
      @endif
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('articles.premium')}}">Premium artikelen<span class="sr-only">(current)</span></a>
      </li>
       @if (Auth::check() && Auth::user()->newsletter === 0) 
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('articles.newsletter')}}">Registreer voor de nieuwsbrief! <span class="sr-only">(current)</span></a>
      </li>
      @endif
      @guest 
      <li class="nav-item">
        <a class="nav-link" href="{{ route('articles.login')}}">Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('register')}}">Registreren</a>
      </li>
      @endguest
        @auth
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Opties
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ route('articles.login')}}">Eigen artikelen weergeven</a>
          <a class="dropdown-item" href="{{ route('articles.create')}}">Nieuw artikel</a>
          <div class="dropdown-divider"></div>
          <form method ="POST" action="{{ route('logout') }}" class="dropdown-item">
        @csrf 
        <button type="submit">Log uit</button>
        </form>
        </div>
        @endauth
      </li>
      
    </ul>
 
    </form>
  </div>
</nav>










<body>
    @yield ('content')
</body>
<script src="{{ asset('js/app.js') }}" defer></script>
</html>