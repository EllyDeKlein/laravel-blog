@extends ('layouts/app')
@section ('content')
<h1> Email </h1>
<form method ="POST" action="/articles/email/send">
        @csrf 
        <div class="field">
         
        <div class="field">
            <label class="label" for="email">Email</label>
        <div class ="control">
            <textarea id="email" name="email" rows="4" cols="50">Typ je email
            </textarea>
             @if ($errors->has('email'))
             <p>{{$errors->first('email')}}</p>
             @endif 
        </div>
        <button type="submit">Voeg toe</button>
        </div>
        @csrf 
        </form>
@endsection