@extends ('layouts/app')
@section ('content')
    <h1>Meld je aan voor de nieuwsbrief </h1> 
@auth 
    <p>Nieuwsbrief voor: {{Auth::user()->name}}</p>
@endauth
    <form method="POST" action="/articles/newsletter/register" id="newsletter">
        @csrf 
        <div class="form-group row">
        <button type="submit">Meld aan!</button>
    </form>
@endsection