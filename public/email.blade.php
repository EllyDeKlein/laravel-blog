@component('mail::message')
# Hello
There are new articles for you

@foreach($articles as $article)
Title: {{ $article->title }}


@endforeach

@component('mail::button', ['url' => route('articles.index')])
View Articles
@endcomponent

Thanks,

{{ config('app.name') }}
@endcomponent